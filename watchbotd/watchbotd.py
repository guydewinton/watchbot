import os
import sys
import time
import threading

from src.library.helpers.observers.close_observer import close_observers
from src.library.helpers.observers.init_observer import init_observer
from src.library.helpers.queuing.watchbot_queue import init_queue
from src.library.helpers.socket.server import socket_connection, socket_listener


os.path.dirname(os.path.abspath(__file__))
sys.path.append('/usr/local/etc/watchbot/')

s_listener = socket_listener('localhost', 6660, 5)

init_queue()
init_observer()

if __name__ == "__main__":
    try:
        while True:
            print('Listening...')
            connection, address = s_listener.accept()
            socket_listener_thread = threading.Thread(target=socket_connection, args=[connection])
            socket_listener_thread.start()
            time.sleep(1)
    except KeyboardInterrupt:
        close_observers()
        s_listener.close()
