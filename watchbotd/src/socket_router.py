from src.modules.del_all.del_all_cli_router import del_all_cli_router
from src.modules.import_json.import_cli_router import import_cli_router
from src.modules.json.json_cli_router import json_cli_router
from src.modules.log.log_cli_router import log_cli_router
from src.modules.log_del.log_del_cli_router import log_del_cli_router
from src.modules.status.status_cli_router import status_cli_router
from src.modules.trigger.trigger_cli_router import trigger_cli_router
from src.modules.trigger_del.trigger_del_cli_router import trigger_del_cli_router
from src.modules.trigger_del_all.trigger_del_all_cli_router import trigger_del_all_cli_router
from src.modules.trigger_list.trigger_list_cli_router import trigger_list_cli_router
from src.modules.watch.watch_cli_router import watch_cli_router
from src.modules.watch_del.watch_del_cli_router import watch_del_cli_router
from src.modules.watch_del_all.watch_del_all_cli_router import watch_del_all_cli_router
from src.modules.watch_list.watch_list_cli_router import watch_list_cli_router
from src.modules.watchtrigger.watchtrigger_cli_router import watchtrigger_cli_router
from src.modules.watchtrigger_del.watchtrigger_del_cli_router import watchtrigger_del_cli_router
from src.modules.watchtrigger_del_all.watchtrigger_del_all_cli_router import watchtrigger_del_all_cli_router
from src.modules.watchtrigger_list.watchtrigger_list_cli_router import watchtrigger_list_cli_router


def socket_router(args):

    def test_func(arguments):
        arguments.server_response = 'some better spelt string'

    routes = {
        'del-all': del_all_cli_router,
        'import-json': import_cli_router,
        'export-json': json_cli_router,
        'log': log_cli_router,
        'log-del': log_del_cli_router,
        'status': status_cli_router,
        'trigger': trigger_cli_router,
        'trigger-del': trigger_del_cli_router,
        'trigger-del_all': trigger_del_all_cli_router,
        'trigger-list': trigger_list_cli_router,
        'watch': watch_cli_router,
        'watch-del': watch_del_cli_router,
        'watch-del_all': watch_del_all_cli_router,
        'watch-list': watch_list_cli_router,
        'watchtrigger': watchtrigger_cli_router,
        'watchtrigger-del': watchtrigger_del_cli_router,
        'watchtrigger-del-all': watchtrigger_del_all_cli_router,
        'watchtrigger-list': watchtrigger_list_cli_router,
        'test': test_func
    }

    routes[args.route](args)

