from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import read_log, clear_log


def log_del_cli_router(args):

    log = read_log()

    process_user_options_server(args)

    user_log = filter_list_by_user(log, args.user)

    delete_count = 0

    args.compile_print('')

    for log_item in user_log:
        if args.name:
            if log_item['trigger_id'] == args.name:
                clear_log(args, args.name)
                delete_count += 1
        else:
            clear_log(args, log_id=log_item['id'])
            delete_count += 1

    if delete_count == 0:
        if args.name:
            args.compile_print('\nTrigger ID not found. No log entries deleted.\n')
        else:
            args.compile_print('\nNo log entries deleted.\n')
    else:
        args.compile_print(f"\n{delete_count} log entries deleted.\n")


