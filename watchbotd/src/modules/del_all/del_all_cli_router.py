from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server
from src.library.helpers.sqlite.sqlite import delete_watch_items, delete_trigger_items, clear_log, read_watch_items, \
    read_trigger_items, read_log


def del_all_cli_router(args):

    process_user_options_server(args)

    null_del = []

    args.compile_print('')

    watch_items = filter_list_by_user(read_watch_items(), args.user)

    if len(watch_items) > 0:
        for watch_item in watch_items:
            delete_watch_items(watch_item['id'])
    else:
        null_del.append(True)

    trigger_items = filter_list_by_user(read_trigger_items(), args.user)

    if len(trigger_items) > 0:
        for trigger_item in trigger_items:
            delete_trigger_items(args, trigger_item['id'])
    else:
        null_del.append(True)

    log = filter_list_by_user(read_log(), args.user)

    if len(log) > 0:
        for log_item in log:
            clear_log(args, log_id=log_item['id'])
    else:
        null_del.append(True)

    if len(null_del) == 3:
        args.compile_print('No watches, triggers or log entries to delete.')
    else:
        args.compile_print('\nAll watches, triggers and log entries deleted.')

    args.compile_print('')
