from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import delete_trigger_items, read_trigger_items


def trigger_del_all_cli_router(args):

    process_user_options_server(args)

    trigger_items = filter_list_by_user(read_trigger_items(), args.user)

    trigger_count = 0

    for trigger_item in trigger_items:
        delete_trigger_items(args, trigger_item['id'])
        trigger_count += 1

    args.compile_print(f"\n{trigger_count} triggers deleted\n")




