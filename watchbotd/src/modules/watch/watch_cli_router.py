import sys


from src.library.helpers.sqlite.sqlite import create_watch_item


def watch_cli_router(args):

    if args.name:
        for char in args.name:
            if char == ' ':
                args.compile_print('Watch namespace may not contain spaces.')
                sys.exit()

    create_watch_item(args)

    args.compile_print('')