import json
import os
from os.path import isdir
from pprint import pprint

from src.library.helpers.data.data import watch_item_dict_packer

from src.library.helpers.sqlite.sqlite import read_trigger_items, read_watch_items, read_log


def json_cli_router(args):

    trigger_items = read_trigger_items()
    watch_items = read_watch_items()
    log = read_log()

    dict_bundle = {}

    if args.nest:
        dict_bundle = {
            'watches': []
        }
        for wi, watch_item in enumerate(watch_items):
            dict_bundle = watch_item_dict_packer(watch_item, wi, dict_bundle)
            dict_bundle['watches'][wi]['triggers'] = []
            ti = 0
            for trigger_item in trigger_items:
                if trigger_item['watch_id'] == watch_item['id']:
                    dict_bundle['watches'][wi]['triggers'].append(trigger_item)
                    dict_bundle['watches'][wi]['triggers'][ti]['command'] = json.loads(trigger_item['command'])
                    dict_bundle['watches'][wi]['triggers'][ti]['log'] = []
                    for log_item in log:
                        if log_item['trigger_id'] == trigger_item['id']:
                            dict_bundle['watches'][wi]['triggers'][ti]['log'].append(log_item)
                    ti += 1
    else:
        dict_bundle = {
            'triggers': [],
            'watches': [],
            'log': [],
        }
        for wi, watch_item in enumerate(watch_items):
            dict_bundle = watch_item_dict_packer(watch_item, wi, dict_bundle)
        for ti, trigger_item in enumerate(trigger_items):
            dict_bundle['triggers'].append(trigger_item)
            dict_bundle['triggers'][ti]['command'] = json.loads(trigger_item['command'])
        for log_item in log:
            dict_bundle['log'].append(log_item)

    if args.file_path:
        path = os.path.abspath(args.file_path)
        if isdir(os.path.dirname(path)):
            write_file = open(args.file_path, 'w')
            json.dump(dict_bundle, write_file, indent=3)
            write_file.close()
        else:
            args.compile_print('\nFile path not in valid directory.\n')

    pprint(
        dict_bundle
    )
