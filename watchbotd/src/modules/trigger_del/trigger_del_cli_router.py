from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import delete_trigger_items, read_trigger_items


def trigger_del_cli_router(args):

    process_user_options_server(args)

    trigger_items = filter_list_by_user(read_trigger_items(), args.user, args.trigger_id)

    trigger_count = 0

    for trigger_item in trigger_items:
        delete_trigger_items(args, trigger_item['id'])
        trigger_count += 1

    if trigger_count == 0:
        args.compile_print(f"Trigger id {args.trigger_id} not found for user {args.user}.")
    else:
        args.compile_print(f"\n{trigger_count} triggers deleted\n")