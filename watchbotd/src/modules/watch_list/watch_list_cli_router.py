from src.library.helpers.console.print_list import watch_console, trigger_console, log_console
from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import read_watch_items, read_trigger_items, read_log


def watch_list_cli_router(args):

    process_user_options_server(args)

    watch_items = []

    if args.watch_id:
        if args.sudo:
            watch_items = read_watch_item()
        else:
            watch_items = filter_list_by_user(read_watch_item(), args.user)
    else:
        if args.sudo:
            watch_items = read_watch_items()
        else:
            watch_items = filter_list_by_user(read_watch_items(), args.user)

    if args.watch_id:
        if len(watch_items) == 0:
            args.raise_error('Invalid watch id.')

    trigger_items = []
    log = []

    if args.triggers or args.log:
        trigger_items = filter_list_by_user(read_trigger_items(), args.user)

    if args.log:
        log = filter_list_by_user(read_log(), args.user)

    if len(watch_items) > 0:
        for i, watch_item in enumerate(watch_items):
            if not args.watch_id:
                args.compile_print(f"#{i}:")
            watch_console(watch_item, 3, args)
            if args.triggers or args.log:
                log_space = 3
                trigger_iteration = 0
                if args.triggers:
                    args.compile_print('   Triggers:')
                for trigger_item in trigger_items:
                    if trigger_item['watch_id'] == watch_item['id']:
                        if args.triggers:
                            if trigger_iteration == 0:
                                args.compile_print('      No triggers associated with this watch.')
                            else:
                                log_space = 6
                                args.compile_print(f"      #{trigger_iteration}")
                                trigger_console(trigger_item, 9, args)
                                trigger_iteration += 1
                        if args.log:
                            log_iteration = 0
                            args.compile_print(f"{' ' * (log_space + 3)}Log:")
                            for log_item in log:
                                if log_item['trigger_id'] == trigger_item['id']:
                                    args.compile_print(f"{' ' * (log_space + 6)}#{log_iteration}")
                                    log_console(log_item, log_space + 9, args)
                                    log_iteration += 1
                            if log_iteration == 0:
                                args.compile_print(f"{' ' * (log_space + 6)}No log entries recorded.")
            args.compile_print('')
    else:
        args.compile_print('No watches registered.\n')
