import json
import os

from src.library.helpers.data.data import introspect_import_dict

from src.library.helpers.sqlite.sqlite import read_trigger_items, read_watch_items, read_log, import_dict


def import_cli_router(args):

    existing_watch_items = read_watch_items()
    existing_trigger_items = read_trigger_items()
    existing_log_items = read_log()

    if os.path.exists(os.path.abspath(args.file_path)):
        json_file = json.loads(open(args.file_path).read())
        introspect_import_dict(json_file, existing_watch_items, existing_trigger_items, existing_log_items, args)
        import_dict(json_file)
    else:
        args.raise_error('Invalid file path.')

