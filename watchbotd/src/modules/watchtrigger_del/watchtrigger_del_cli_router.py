from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import delete_watch_items, delete_trigger_items, \
    read_watch_item, read_trigger_items


def watchtrigger_del_cli_router(args):

    process_user_options_server(args)

    triggers = filter_list_by_user(read_trigger_items(), args.user, args.watchtrigger_id)

    matched_watches = filter_list_by_user(read_watch_item(args.watchtrigger_id), args.user, args.watchtrigger_id)
    matched_triggers = []
    trigger_match = []
    for trigger in triggers:
        if trigger['watch_id'] == args.watchtrigger_id:
            matched_triggers.append(0)
        if trigger['id'] == args.watchtrigger_id:
            trigger_match.append(0)
    # TODO: redundant sql queries - delete also queries

    if len(matched_watches) < 1 and len(trigger_match) < 1:
        args.compile_print(f"There is no watch or trigger with the of id of '{args.watchtrigger_id}'.")
        args.compile_print('Cannot delete this watch/trigger combination.')
    elif len(matched_watches) < 1:
        args.compile_print(f"There is no watch with the of id of '{args.watchtrigger_id}'.")
        args.compile_print('Cannot delete this watch/trigger combination.')
    elif len(trigger_match) < 1:
        args.compile_print(f"There is no trigger with the of id '{args.watchtrigger_id}'.")
        args.compile_print('Cannot delete this watch/trigger combination.')
    else:
        if len(matched_triggers) > 1:
            args.compile_print(f"There is more than trigger associated with the watch of id '{args.watchtrigger_id}'.")
            args.compile_print('Cannot delete this watch/trigger combination.')
        elif len(matched_triggers) < 1:
            args.compile_print(f"There is no trigger associated with the watch of id '{args.watchtrigger_id}'.")
            args.compile_print('Cannot delete this watch/trigger combination.')

        else:
            delete_watch_items(args, args.watchtrigger_id)
            # delete_trigger_items(args.watchtrigger_id)
