

from src.library.helpers.sqlite.sqlite import create_trigger_item


def trigger_cli_router(args):

    args.src_path = None

    if args.name:
        for char in args.name:
            if char == ' ':
                args.raise_error('Trigger namespace may not contain spaces.')

    create_trigger_item(args)

    args.compile_print('')
