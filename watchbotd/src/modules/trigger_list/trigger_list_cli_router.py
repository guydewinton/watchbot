from src.library.helpers.console.print_list import trigger_console, log_console
from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import read_trigger_items, read_log, read_trigger_item


def trigger_list_cli_router(args):

    process_user_options_server(args)

    trigger_items = []

    if args.trigger_id:
        if args.sudo:
            trigger_items = read_trigger_item(args.trigger_id)
        else:
            trigger_items = filter_list_by_user(read_trigger_item(args.trigger_id), args.user)
    else:
        if args.sudo:
            trigger_items = read_trigger_items()
        else:
            trigger_items = filter_list_by_user(read_trigger_items(), args.user)

    if args.trigger_id:
        if len(trigger_items) == 0:
            args.raise_error('Invalid trigger id.')

    log = []

    if args.log:
        log = filter_list_by_user(read_log(), args.user)

    if len(trigger_items) > 0:
        for i, trigger_item in enumerate(trigger_items):
            if not args.trigger_id:
                args.compile_print(f"#{i}:")
            trigger_console(trigger_item, 3, args)
            if args.log:
                log_iteration = 0
                args.compile_print(f"{' ' * 3}Log:")
                for log_item in log:
                    if log_item['trigger_id'] == trigger_item['id']:
                        args.compile_print(f"{' ' * 6}#{log_iteration}")
                        log_console(log_item, 9, args)
                        log_iteration += 1
                if log_iteration == 0:
                    args.compile_print(f"{' ' * 6}No log entries recorded.\n")
            args.compile_print('')
    else:
        args.compile_print('No trigger registered.\n')