from src.library.helpers.data.filter_list_by_user import filter_list_by_user
from src.library.helpers.data.user_process import process_user_options_server

from src.library.helpers.sqlite.sqlite import delete_watch_items, read_trigger_items, read_watch_items


def watchtrigger_del_all_cli_router(args):

    current_user = process_user_options_server(args)

    triggers = filter_list_by_user(read_trigger_items(), current_user[0])
    watches = filter_list_by_user(read_watch_items(), current_user[0])

    matched_watchtriggers = []

    for watch in watches:
        for trigger in triggers:
            if watch['id'] == trigger['id']:
                matched_watchtriggers.append(watch['id'])

    if len(matched_watchtriggers) == 0:
        args.compile_print('There are no watchtriggers registered.\n')
    else:
        for matched_watch in matched_watchtriggers:
            delete_watch_items(args, matched_watch)
            args.compile_print(f"Deleted watchtrigger {matched_watch}")
        args.compile_print('')
