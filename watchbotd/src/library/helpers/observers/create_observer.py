from src.library.helpers.schedulers import init_scheduler
from src.library.helpers.schedulers.schedule_dict import schedule_dict


def create_observer(trigger_item, args):
    if trigger_item['watch_id'] in schedule_dict:
        schedule_dict[trigger_item['watch_id']]['handler'].add_trigger(trigger_item)
    else:
        watch_item = args.watch
        init_scheduler.init_scheduler(watch_item, [trigger_item])
