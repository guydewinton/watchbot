from src.library.helpers.schedulers.schedule_dict import schedule_dict


def close_observers():
    for observer_key, observer_dict in schedule_dict.items():
        observer_dict['observer'].stop()
        observer_dict['observer'].join(None)
