from src.library.helpers.schedulers import init_scheduler
from src.library.helpers.sqlite import sqlite


def init_observer():
    watch_items = sqlite.read_watch_items()
    trigger_items = sqlite.read_trigger_items()
    watch_triggers = []
    for watch_item in watch_items:
        for trigger_item in trigger_items:
            if watch_item['id'] == trigger_item['watch_id']:
                watch_triggers.append(trigger_item)
        if len(watch_triggers) > 0:
            init_scheduler.init_scheduler(watch_item, watch_triggers)
