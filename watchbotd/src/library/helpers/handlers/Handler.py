import os
import uuid

import watchdog.events
import watchdog.observers

from src.library.helpers.queuing.watchbot_queue import watchbot_queue


class Handler(watchdog.events.PatternMatchingEventHandler):
    def __init__(self, watch_item, trigger_items):
        self.id = watch_item['id']
        self.watch = watch_item
        self.triggers = {
            'modified': [],
            'deleted': [],
            'created': [],
            'moved': []
        }
        for trigger_item in trigger_items:
            self.triggers[trigger_item['event']].append(trigger_item)
        self.uuid = uuid.uuid4()
        watchdog.events.PatternMatchingEventHandler.__init__(self, patterns=None,
                                                             ignore_directories=True, case_sensitive=False)

    def process_event(self, event, action):
        for trigger in self.triggers[action]:
            watchbot_queue.put([self, event, trigger])

    def add_trigger(self, trigger):
        self.triggers[trigger['event']].append(trigger)

    def on_modified(self, event):
        if os.path.exists(event.src_path):
            self.process_event(event, 'modified')
        else:
            if os.path.isfile(event.src_path):
                self.process_event(event, 'deleted')

    def on_deleted(self, event):
        if os.path.isdir(event.src_path):
            self.process_event(event, 'deleted')

    def on_created(self, event):
        self.process_event(event, 'created')

    def on_moved(self, event):
        self.process_event(event, 'moved')
