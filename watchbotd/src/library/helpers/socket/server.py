import json
import socket

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.serialization import load_pem_public_key

from src.library.classes.ArgClass import ArgClass
from src.socket_router import socket_router


def socket_listener(host, port, queue):
    sock = socket.socket()
    sock.bind((host, port))
    sock.listen(queue)
    return sock


def socket_connection(connection):
    client_pub = connection.recv(4096)
    client_public_key = load_pem_public_key(client_pub)
    aes_key = Fernet.generate_key()
    f = Fernet(aes_key)
    encrypted_aes_key = client_public_key.encrypt(
        aes_key,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    connection.send(encrypted_aes_key)
    encrypted_request = connection.recv(4096)
    decrypted_request = f.decrypt(encrypted_request)
    args_dict = json.loads(decrypted_request.decode())
    args = ArgClass(args_dict, connection, aes_key)
    socket_router(args)
    encrypted_response = f.encrypt(bytes(args.server_response, 'utf-8'))
    connection.send(encrypted_response)
    connection.close()
