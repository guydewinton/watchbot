import json
import os

from datetime import datetime

from os_tools.generic import execute_command

from src.library.helpers.sqlite import sqlite


def execute_trigger_command(handler, event, trigger):
    file_path, file_extension = os.path.splitext(event.src_path)
    ext_check = False
    if handler.watch['type'] == 'dir':
        watched_extensions = json.loads(handler.watch['ext'])
        ext_check = False
        if watched_extensions[0] == '*':
            ext_check = True
        else:
            for watched_extension in watched_extensions:
                if watched_extension == file_extension[1:]:
                    ext_check = True
    else:
        ext_check = True
    if ext_check:
        if trigger['sudo'] == 'True':
            user_switcher = ''
        else:
            user_switcher = f"sudo -u {trigger['user']} "
        command = f"{user_switcher}{' '.join(json.loads(trigger['command']))}"
        stdout, stderr, code = execute_command(command, trigger['work_dir'])
        sqlite.update_log(
            trigger['id'],
            stdout.replace("'", "''"),
            stderr.replace("'", "''"),
            str(code),
            str(datetime.now()),
            trigger['user'],
            trigger['sudo']
        )


    # TODO: add specific file that caused the trigger
    # TODO: some kind of infinite loop protector