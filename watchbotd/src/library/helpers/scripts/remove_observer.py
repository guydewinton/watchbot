from src.library.helpers.schedulers.schedule_dict import schedule_dict


def remove_observer(handler):
    watch = schedule_dict[handler.id]['watch']
    observer = schedule_dict[handler.id]['observer']
    observer.unschedule(watch)
    del(schedule_dict[handler.id])
