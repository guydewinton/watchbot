import json


def string_or_list(input_obj, args):
    if type(input_obj) == list:
        return json.dumps(input_obj)
    elif type(input_obj) == str:
        if input_obj[0] == '[' and input_obj[-1] == ']':
            return input_obj
        else:
            return f"['{input_obj}']"
    else:
        args.raise_error('Invalid input.')


def format_string_for_sql(string):
    return string.replace("'", "''")


def comma_seperated_string_to_list(string):
    return string.split(',')


def remove_spaces_from_list_of_strings(value_list):
    word_list = []
    for value in value_list:
        char_list = []
        for char in value:
            if not char == ' ':
                char_list.append(char)
        word_list.append(''.join(char_list))
    return word_list


def watch_item_dict_packer(watch_item, wi, dict_bundle):
    if watch_item['type'] == 'dir':
        dict_bundle['watches'].append(watch_item)
        dict_bundle['watches'][wi]['ext'] = json.loads(watch_item['ext'])
    else:
        dict_bundle['watches'].append(
            {
                'id': watch_item['id'],
                'path': watch_item['path'],
                'type': watch_item['type'],
                'timestamp': watch_item['timestamp']
            }
        )
    return dict_bundle


def introspect_import_dict(import_dict, existing_watch_items, existing_trigger_items, existing_log_items, args):

    def check_log(log):
        for log_item in log:
            if log['id']:
                for existing_log_item in existing_log_items:
                    if existing_log_item['id'] == log_item['id']:
                        args.raise_error(f"Log item with ID {log_item['id']} already exists.")
            else:
                args.raise_error('Log item missing ID field.')
            if 'timestamp' not in log:
                args.raise_error(f"Log item {log_item['id']} missing timestamp field.")
            if 'trigger_id' not in log:
                args.raise_error(f"Log item {log_item['id']} missing trigger_id field.")
            if 'stdout' not in log:
                args.raise_error(f"Log item {log_item['id']} missing stdout field.")
            if 'stderr' not in log:
                args.raise_error(f"Log item {log_item['id']} missing stderr field.")
            if 'exit_code' not in log:
                args.raise_error(f"Log item {log_item['id']} missing exit_code field.")

    def check_triggers(triggers):
        for trigger_item in triggers:
            if 'id' in trigger_item:
                for existing_trigger_item in existing_trigger_items:
                    if existing_trigger_item['id'] == trigger_item['id']:
                        args.raise_error(f"Trigger item with ID {trigger_item['id']} already exists.")
            else:
                args.raise_error('Trigger item missing ID field.')
            if 'timestamp' not in trigger_item:
                args.raise_error(f"Trigger item {trigger_item['id']} missing timestamp field.")
            if 'watch_id' not in  trigger_item:
                args.raise_error(f"Trigger item {trigger_item['id']} missing watch_id field.")
            if 'event' not in trigger_item:
                args.raise_error(f"Trigger item {trigger_item['id']} missing event field.")
            if 'work_dir' not in trigger_item:
                args.raise_error(f"Trigger item {trigger_item['id']} missing work_dir field.")
            if 'command' not in trigger_item:
                args.raise_error(f"Trigger item {trigger_item['id']} missing command field.")
            if 'log' in trigger_item:
                check_log(trigger_item['log'])

    def check_watches(watches):
        for watch_item in watches:
            if watch_item['id']:
                for existing_watch_item in existing_watch_items:
                    if existing_watch_item['id'] == watch_item['id']:
                        args.raise_error(f"Watch item with ID {watch_item['id']} already exists.")
            else:
                args.raise_error('Watch item missing ID field.')
            if 'timestamp' not in watch_item:
                args.raise_error(f"Watch item {watch_item['id']} missing timestamp field.")
            if 'path' not in watch_item:
                args.raise_error(f"Watch item {watch_item['id']} missing path field.")
            if 'type' not in watch_item:
                args.raise_error(f"Watch item {watch_item['id']} missing type field.")
            else:
                if watch_item['type'] == 'dir':
                    if 'ext' not in watch_item:
                        args.raise_error(f"Watch item {watch_item['id']} missing ext field.")
                    if 'recursive' not in  watch_item:
                        args.raise_error(f"Watch item {watch_item['id']} missing recursive field.")
            if 'triggers' in watch_item:
                check_triggers(watch_item['triggers'])

    if 'watches' in import_dict:
        check_watches(import_dict['watches'])

    elif 'triggers' in import_dict:
        check_triggers(import_dict['triggers'])

    elif 'log' in import_dict:
        check_log(import_dict['log'])

    else:
        args.raise_error('JSON does not contain any watchbot data.')
