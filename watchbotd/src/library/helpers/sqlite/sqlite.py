import json
from datetime import datetime

from sqlite3_api.base_class import SqliteManager

from db_schema import watchbot_schema
import config

from src.library.helpers.observers.create_observer import create_observer
from src.library.helpers.sqlite.options import watch_options, trigger_options


def watchbot_sql_connection():
    db_path = f"{config.data_dir}{config.watchbot_db}"
    return SqliteManager(watchbot_schema, db_path)


def update_log(trigger_id, stdout, stderr, exit_code, timestamp, user, sudo):
    watchbot_db = watchbot_sql_connection()
    watchbot_db.create_records(
        'log',
        {
            'trigger_id': trigger_id,
            'stdout': stdout,
            'stderr': stderr,
            'exit_code': exit_code,
            'timestamp': timestamp,
            'user': user,
            'sudo': sudo
        }
    )


def introspect_watch_db():
    watchbot_db = watchbot_sql_connection()
    return watchbot_db.validate_db_schema()


def read_watch_items():
    watchbot_db = watchbot_sql_connection()
    return watchbot_db.read_records('watch', '*')


def read_watch_item(watch_id):
    watchbot_db = watchbot_sql_connection()
    return watchbot_db.read_records('watch', '*', {'id': watch_id})


def read_trigger_items():
    watchbot_db = watchbot_sql_connection()
    return watchbot_db.read_records('trigger', '*')


def read_trigger_item(trigger_id):
    watchbot_db = watchbot_sql_connection()
    return watchbot_db.read_records('trigger', '*', {'id': trigger_id})


def read_log(namespace=None):
    watchbot_db = watchbot_sql_connection()
    if namespace:
        return watchbot_db.read_records('log', '*', {'trigger_id': namespace})
    else:
        return watchbot_db.read_records('log', '*')


def create_watch_item(args):
    watch_id, src_dir, watch_type, options = watch_options(args)
    if len(read_watch_item(watch_id)) == 0:
        watchbot_db = watchbot_sql_connection()
        watch = {
                'timestamp': str(datetime.now()),
                'path': src_dir,
                'type': watch_type,
                **options
            }
        watchbot_db.create_records(
            'watch',
            watch,
            watch_id
        )
        args.compile_print('Watch Created')
        args.compile_print(watch_id)
    else:
        args.raise_error(f"Watch name '{watch_id}' already exists.")


def create_trigger_item(args):
    trigger_id, watch_id, command, options = trigger_options(args)
    if len(read_trigger_item(trigger_id)) == 0:
        watch_db = watchbot_sql_connection()
        trigger = {
            'timestamp': str(datetime.now()),
            'watch_id': watch_id,
            'command': command,
            'user': args.user,
            **options
        }
        create_observer(trigger, args)
        watch_db.create_records(
            'trigger',
            trigger,
            trigger_id
        )
        args.compile_print('Trigger Created')
        args.compile_print(trigger_id)
    else:
        args.raise_error(f"Trigger name '{trigger_id}' already exists.")


def delete_watch_items(args, watch_id=None):
    watchbot_db = watchbot_sql_connection()
    triggers = read_trigger_items()
    if watch_id:
        if watchbot_db.delete_records('watch', {'id': watch_id}):
            args.compile_print(f"Watch {watch_id} deleted.")
            for trigger in triggers:
                if trigger['watch_id'] == watch_id:
                    watchbot_db.delete_records('trigger', {'id': trigger['id']})
                    args.compile_print(f"Trigger {trigger['id']} associated with watch {watch_id} deleted.")
            return True
        else:
            args.compile_print(f"{watch_id}: Invalid watch id.")
            return False
    else:
        watch_items = read_watch_items()
        for watch_item in watch_items:
            watchbot_db.delete_records('watch', {'id': watch_item['id']})
            args.compile_print(f"Watch {watch_item['id']} deleted.")
            for trigger in triggers:
                if trigger['watch_id'] == watch_item['id']:
                    watchbot_db.delete_records('trigger', {'id': trigger['id']})
                    args.compile_print(f"Trigger {trigger['id']} associated with watch {watch_item['id']} deleted.")


def delete_trigger_items(args, trigger_id=None):
    watch_db = watchbot_sql_connection()
    if trigger_id:
        if watch_db.delete_records('trigger', {'id': trigger_id}):
            args.compile_print(f"Trigger {trigger_id} deleted.")
            return True
        else:
            args.compile_print(f"{trigger_id}: Invalid trigger id.")
            return False
    else:
        trigger_items = read_trigger_items()
        for trigger_item in trigger_items:
            watch_db.delete_records('trigger', {'id': trigger_item['id']})
            args.compile_print(f"Trigger {trigger_item['id']} deleted.")


def clear_log(args, trigger_id=None, log_id=None):
    log = read_log()
    watchbot_db = watchbot_sql_connection()
    if trigger_id:
        for log_item in log:
            if log_item['trigger_id'] == trigger_id:
                watchbot_db.delete_records('log', {'id': log_item['id']})
                args.compile_print(f"Log {log_item['id']} deleted")
    elif log_id:
        for log_item in log:
            if log_item['id'] == log_id:
                watchbot_db.delete_records('log', {'id': log_item['id']})
                args.compile_print(f"Log {log_item['id']} deleted")
    else:
        for log_item in log:
            watchbot_db.delete_records('log', {'id': log_item['id']})
            print(f"Log {log_item['id']} deleted")


def import_dict(import_object):

    def import_log(log):
        log_db = watchbot_sql_connection()
        for log_item in log:
            log_db.create_records(
                'log',
                {
                    'timestamp': log_item['timestamp'],
                    'trigger_id': log_item['trigger_id'],
                    'stout': log_item['stout'],
                    'stderr': log_item['stderr'],
                    'exit_code': log_item['exit_code'],
                },
            )

    def import_triggers(triggers):
        watch_db = watchbot_sql_connection()
        for trigger_item in triggers:
            if 'log' in trigger_item:
                import_log(trigger_item['log'])
            watch_db.create_records(
                'trigger',
                {
                    'timestamp': trigger_item['timestamp'],
                    'watch_id': trigger_item['watch_id'],
                    'event': trigger_item['event'],
                    'work_dir': trigger_item['work_dir'],
                    'command': json.dumps(trigger_item['command']),
                },
                trigger_item['id']
            )

    def import_watches(watches):
        watchbot_db = watchbot_sql_connection()
        for watch_item in watches:
            field_dict = {}
            if watch_item['type'] == 'dir':
                field_dict['ext'] = json.dumps(watch_item['ext'])
                field_dict['recursive'] = watch_item['recursive']
            if 'triggers' in watch_item:
                import_triggers(watch_item['triggers'])
            watchbot_db.create_records(
                'watch',
                {
                    'timestamp': watch_item['timestamp'],
                    'path': watch_item['path'],
                    'type': watch_item['type'],
                    **field_dict
                },
                watch_item['id']
            )
    if 'watches' in import_object:
        import_watches(import_object['watches'])
    if 'triggers' in import_object:
        import_triggers(import_object['triggers'])
    if 'log' in import_object:
        import_log(import_object['log'])