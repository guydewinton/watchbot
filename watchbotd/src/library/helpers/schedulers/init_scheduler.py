from watchdog import observers

from src.library.helpers.handlers.Handler import Handler
from src.library.helpers.schedulers.schedule_dict import schedule_dict


def init_scheduler(watch_item, trigger_items):
    event_handler = Handler(watch_item, trigger_items)
    observer = observers.Observer()
    if watch_item['recursive'] == 'True':
        recursive = True
    else:
        recursive = False
    watch = observer.schedule(event_handler, path=watch_item['path'], recursive=recursive)
    observer.start()
    schedule_dict[watch_item['id']] = {
        'observer': observer,
        'watch': watch,
        'handler': event_handler,
    }
