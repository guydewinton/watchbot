import sys


def raise_error(warning, args):
    args.server_response = f"{args.server_response}\n\nError: {warning}\nOperation cancelled\n"
    args.server_connection.send(bytes(args.server_response, 'utf-8'))
    args.server_connection.close()
