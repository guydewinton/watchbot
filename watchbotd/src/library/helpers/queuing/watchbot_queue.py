import queue
import threading

from src.library.helpers.queuing.process_queue import process_load_queue

watchbot_queue = queue.Queue(0)


def init_queue():
    worker = threading.Thread(target=process_load_queue, args=[watchbot_queue])
    worker.setDaemon(True)
    worker.start()
