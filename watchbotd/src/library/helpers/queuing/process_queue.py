import time
import threading

from src.library.helpers.scripts.execute_trigger_command import execute_trigger_command


def process_load_queue(queue):
    while True:
        if not queue.empty():
            handler, event, trigger = queue.get()
            execute_trigger_command_thread = threading.Thread(
                target=execute_trigger_command,
                args=[handler, event, trigger]
            )
            execute_trigger_command_thread.start()
            execute_trigger_command_thread.join()
        else:
            time.sleep(1)
