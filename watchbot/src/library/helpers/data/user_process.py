from getpass import getpass
import uuid

from os_tools.generic import check_user_sudo, check_user_in_group
from os_tools.modules.linux.check_user_exists import check_user_exists
from os_tools.modules.linux.get_current_user import get_current_user

from src.library.helpers.console.raise_error import raise_error


def process_user_options_client(args):
    current_user = get_current_user()
    password_check = False
    if hasattr(args, 'sudo'):
        if args.sudo:
            if check_user_sudo(current_user):
                args.password = getpass(f"\nEnter password for user {current_user}: ")
                password_check = True
                args.auth = 'pass'
            else:
                raise_error('\nThis user does not have sudo privileges.\n')
    else:
        args.sudo = False
    if not hasattr(args, 'user'):
        args.user = None
    if args.user:
        if check_user_exists(args.user):
            if not args.user == current_user:
                if not password_check:
                    args.password = getpass(f"\nEnter password for user {args.user}: ")
                    password_check = True
                    args.auth = 'pass'
        else:
            raise_error('\nUser does not exist.\n')
    else:
        args.user = current_user
        if not password_check:
            if check_user_in_group(args.user, 'watchbot'):
                unique_string = str(uuid.uuid4())
                path = f"/usr/local/lib/watchbot/perms/{unique_string[0:8]}{args.user}"
                with open(path, 'w') as file:
                    file.write(unique_string)
                    file.close()
                    args.password = unique_string
                    args.auth = 'cert'
            else:
                args.password = getpass(f"\nUser not a member of watchbot group."
                                        f"\nTo use watchbot without a password, add user to watchbot group."
                                        f"\n(requires user session to be restarted to take effect.)\n"
                                        f"\nEnter password for user {args.user}: ")
                args.auth = 'password'
