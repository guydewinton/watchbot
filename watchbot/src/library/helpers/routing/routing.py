import argparse
import json

from src.library.helpers.data.user_process import process_user_options_client
from src.library.helpers.socket.client import socket_connection


def cli_switch_router(argparser, switcher, router_args):
    if len(router_args[0]) > 0:
        current_arg = router_args[0][0]
        router_args[1].append(current_arg)
        router_args[0].pop(0)
        if current_arg == '-h':
            argparser.parse_args()
        else:
            try:
                switcher[current_arg]['route'](argparser, switcher[current_arg]['parser'], router_args)
            except KeyError:
                print('''
--------------------
Error: Invalid input
--------------------
                ''')
                router_args[1].pop(-1)
                argparser.parse_args([*router_args[1], '-h'])
    else:
        print('''
-----------------------------
Error: Insufficient arguments
-----------------------------
        ''')
        argparser.parse_args([*router_args[1], '-h'])


def cli_route_terminator(argparser):
    return argparser.parse_args()


class SubcommandHelpFormatter(argparse.RawDescriptionHelpFormatter):
    def _format_action(self, action):
        parts = super(argparse.RawDescriptionHelpFormatter, self)._format_action(action)
        if action.nargs == argparse.PARSER:
            parts = "\n".join(parts.split("\n")[1:])
        return parts


def socket_router(args, route):
    process_user_options_client(args)
    args.route = route
    server_response = socket_connection(json.dumps(vars(args)))
    print(server_response[1].decode())
