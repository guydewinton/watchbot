import socket

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding

from src.library.helpers.console.raise_error import raise_error


def socket_connection(packet):
    s_connection = socket.socket()
    try:
        s_connection.connect(('localhost', 6660))
        client_private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=4096,
        )
        client_public_key = client_private_key.public_key()
        pem = client_public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        s_connection.send(pem)
        aes_key_encrypted = s_connection.recv(4096)
        aes_key = client_private_key.decrypt(
            aes_key_encrypted,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        f = Fernet(aes_key)
        token = f.encrypt(bytes(packet, 'utf-8'))
        s_connection.send(token)
        server_response_encrypted = s_connection.recv(4096)
        server_response = f.decrypt(server_response_encrypted)
        return True, server_response
    except BaseException as exception:
        print(f"\n{exception}\n")
        raise_error('\nThere was a problem connecting the watchbot daemon.\nPlease try again.\n')
