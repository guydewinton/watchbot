from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def trigger_cli_router(argparser, subparser, router_args):
    subparser.add_argument('-n', metavar='<TRIGGER NAME>', dest='name', type=str, help='The namespace of the trigger.')
    subparser.add_argument('-w',  metavar='<WORKING DIRECTORY>', dest='work_dir', type=str, help='The working directory for command.')
    subparser.add_argument('-e',  metavar='<EVENT TYPE(S)>', dest='event', type=str, help='Event types to respond to.')
    subparser.add_argument('-s', action='store_true', dest='sudo', help='Execute trigger command as sudo.')
    subparser.add_argument('-u',  metavar='<USERNAME>', dest='user', type=str, help='Run trigger command as user.')
    subparser.add_argument(metavar='<WATCH ID>', dest='watch_id', type=str, help='The file/directory path to watch.')
    subparser.add_argument(metavar='<COMMAND>', nargs='+', dest='command', type=str, help='The command to execute.')

    args = cli_route_terminator(argparser)

    socket_router(args, router_args[1][0])
