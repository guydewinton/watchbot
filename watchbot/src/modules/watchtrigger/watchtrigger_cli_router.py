import argparse

from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def watchtrigger_cli_router(argparser, subparser, router_args):

    subparser.add_argument('-n', metavar='<WATCH/TRIGGER NAME>', dest='name', type=str, help='The namespace of the watch and trigger.')
    subparser.add_argument('-r', action='store_true', dest='recursive', help='Recursively watch directories.')
    subparser.add_argument('-x', metavar='<FILE EXTENSION>', dest='ext', type=str, help='File extensions of files to watch.')
    subparser.add_argument('-w',  metavar='<WORKING DIRECTORY>', dest='work_dir', type=str, help='The working directory for command.')
    subparser.add_argument('-e',  metavar='<EVENT TYPE(S)>', dest='event', type=str, help='Event types to respond to.')
    subparser.add_argument('-s', action='store_true', dest='sudo', default=False, help='Execute trigger command as sudo.')
    subparser.add_argument('-u', metavar='<USERNAME>', dest='user', type=str, help='Run trigger command as user.')
    subparser.add_argument(metavar='<WATCH PATH>', dest='src_path', type=str, help='The file/directory path to watch.')
    subparser.add_argument(metavar='<COMMAND>', nargs=argparse.REMAINDER, dest='command', type=str, help='The command to execute.')

    args = cli_route_terminator(argparser)

    socket_router(args, router_args[1][0])