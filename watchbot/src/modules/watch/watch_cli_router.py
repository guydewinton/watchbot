from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def watch_cli_router(argparser, subparser, router_args):
    subparser.add_argument('-n', metavar='<WATCH NAME>', dest='name', type=str,
                           help='The namespace of the watch.')
    subparser.add_argument('-r', action='store_true', dest='recursive', help='Recursively watch directories.')
    subparser.add_argument('-x', metavar='<FILE EXTENSIONS>', dest='ext', type=str,
                           help='File extensions of files to watch. (Comma seperated string)')
    subparser.add_argument('-s', action='store_true', dest='sudo', help='Execute trigger command as sudo.')
    subparser.add_argument('-u',  metavar='<USERNAME>', dest='user', type=str, help='Run trigger command as user.')
    # TODO: improve input method - json list seems awkward - must be '["", ""]'
    subparser.add_argument(metavar='<WATCH PATH>', dest='src_path', type=str, help='The file/directory path to watch.')

    args = cli_route_terminator(argparser)

    socket_router(args, router_args[1][0])
