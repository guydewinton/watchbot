from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def json_cli_router(argparser, subparser, router_args):
    subparser.add_argument('-f', metavar='<PATH>', dest='file_path', type=str, help='The namespace of the watch and trigger')
    subparser.add_argument('-n', action='store_true', dest='nest', help='The namespace of the watch and trigger')

    args = cli_route_terminator(argparser)

    args.sudo = True

    socket_router(args, router_args[1][0])
