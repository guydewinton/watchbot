from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def watch_list_cli_router(argparser, subparser, router_args):
    subparser.add_argument('-n', metavar='<WATCH ID>', dest='watch_id', type=str, help='The id of a watch.')
    subparser.add_argument('-t', action='store_true', dest='triggers', help='Display triggers associated with each watch.')
    subparser.add_argument('-l', action='store_true', dest='log', help='Display log of triggers associated with watch.')
    subparser.add_argument('-s', action='store_true', dest='sudo', help='Display all triggers (requires sudo privileges).')
    subparser.add_argument('-u',  metavar='<USERNAME>', dest='user', type=str, help='Display triggers of specific user.')

    args = cli_route_terminator(argparser)

    socket_router(args, router_args[1][0])
