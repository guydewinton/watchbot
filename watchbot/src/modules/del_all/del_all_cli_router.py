from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def del_all_cli_router(argparser, subparser, router_args):

    subparser.add_argument('-u',  metavar='<USERNAME>', dest='user', type=str, help='Delete everything for specified user.')
    subparser.add_argument('-a', action='store_true', dest='sudo', help='Delete everything for all users (requires admin privileges).')

    args = cli_route_terminator(argparser)

    socket_router(args, router_args[1][0])
