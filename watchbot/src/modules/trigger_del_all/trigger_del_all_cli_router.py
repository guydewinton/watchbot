from src.library.helpers.routing.routing import cli_route_terminator, socket_router


def trigger_del_all_cli_router(argparser, subparser, router_args):
    subparser.add_argument('-s', action='store_true', dest='sudo', help='Execute trigger command as sudo.')
    subparser.add_argument('-u',  metavar='<USERNAME>', dest='user', type=str, help='Run trigger command as user.')

    args = cli_route_terminator(argparser)

    socket_router(args, router_args[1][0])
