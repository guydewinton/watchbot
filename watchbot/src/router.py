import argparse
import sys

from src.library.helpers.routing.routing import cli_switch_router
from src.modules.del_all.del_all_cli_router import del_all_cli_router
from src.modules.import_json.import_cli_router import import_cli_router
from src.modules.log.log_cli_router import log_cli_router
from src.modules.log_del.log_del_cli_router import log_del_cli_router
from src.modules.json.json_cli_router import json_cli_router
from src.modules.status.status_cli_router import status_cli_router
from src.modules.trigger.trigger_cli_router import trigger_cli_router
from src.modules.trigger_del.trigger_del_cli_router import trigger_del_cli_router
from src.modules.trigger_del_all.trigger_del_all_cli_router import trigger_del_all_cli_router
from src.modules.trigger_list.trigger_list_cli_router import trigger_list_cli_router
from src.modules.watch.watch_cli_router import watch_cli_router
from src.modules.watch_del.watch_del_cli_router import watch_del_cli_router
from src.modules.watch_del_all.watch_del_all_cli_router import watch_del_all_cli_router
from src.modules.watch_list.watch_list_cli_router import watch_list_cli_router
from src.modules.watchtrigger.watchtrigger_cli_router import watchtrigger_cli_router
from src.modules.watchtrigger_del.watchtrigger_del_cli_router import watchtrigger_del_cli_router
from src.modules.watchtrigger_del_all.watchtrigger_del_all_cli_router import watchtrigger_del_all_cli_router
from src.modules.watchtrigger_list.watchtrigger_list_cli_router import watchtrigger_list_cli_router


def cli_router():

    # TODO: convert all of this to a class... useful for later me thinks...

    router_args = [[*sys.argv], []]
    router_args[0].pop(0)

    argparser = argparse.ArgumentParser(description='Watchbot CLI utility')

    subparsers = argparser.add_subparsers(title='Root Commands', metavar='<COMMAND>', dest='subparser')

    switcher = {
        'status': {
            'route': status_cli_router,
            'parser': subparsers.add_parser(name='status', help='Get status of watchbot service and databases.'),
        },
        'watch': {
            'route': watch_cli_router,
            'parser': subparsers.add_parser(name='watch', help='Add a watch to filesystem.'),
        },
        'watch-list': {
            'route': watch_list_cli_router,
            'parser': subparsers.add_parser(name='watch-list', help='Display a list of watches.'),
        },
        'watch-del': {
            'route': watch_del_cli_router,
            'parser': subparsers.add_parser(name='watch-del', help='Delete a watch.'),
        },
        'watch-del-all': {
            'route': watch_del_all_cli_router,
            'parser': subparsers.add_parser(name='watch-del-all', help='Delete all watches.'),
        },
        'trigger': {
            'route': trigger_cli_router,
            'parser': subparsers.add_parser(name='trigger', help='Add a trigger event to a watch.'),
        },
        'trigger-list': {
            'route': trigger_list_cli_router,
            'parser': subparsers.add_parser(name='trigger-list', help='Display a list of triggers.'),
        },
        'trigger-del': {
            'route': trigger_del_cli_router,
            'parser': subparsers.add_parser(name='trigger-del', help='Delete a trigger.'),
        },
        'trigger-del-all': {
            'route': trigger_del_all_cli_router,
            'parser': subparsers.add_parser(name='trigger-del-all', help='Delete all triggers.'),
        },
        'watchtrigger': {
            'route': watchtrigger_cli_router,
            'parser': subparsers.add_parser(name='watchtrigger', help='Add a combined watch and trigger.'),
        },
        'watchtrigger-list': {
            'route': watchtrigger_list_cli_router,
            'parser': subparsers.add_parser(name='watchtrigger-list', help='Display a list of watchtriggers.'),
        },
        'watchtrigger-del': {
            'route': watchtrigger_del_cli_router,
            'parser': subparsers.add_parser(name='watchtrigger-del', help='Delete a watchtrigger.'),
        },
        'watchtrigger-del-all': {
            'route': watchtrigger_del_all_cli_router,
            'parser': subparsers.add_parser(name='watchtrigger-del-all', help='Delete all watchtriggers.'),
        },
        'log': {
            'route': log_cli_router,
            'parser': subparsers.add_parser(name='log', help='View log of trigger events.'),
        },
        'log-clear': {
            'route': log_del_cli_router,
            'parser': subparsers.add_parser(name='log-clear', help='Clear the log.'),
        },
        'del-all': {
            'route': del_all_cli_router,
            'parser': subparsers.add_parser(name='del-all', help='Delete all watches, triggers and log records.'),
        },
        'json': {
            'route': json_cli_router,
            'parser': subparsers.add_parser(name='export-json', help='Print or export JSON string (requires admin privileges).'),
        },
        'import': {
            'route': import_cli_router,
            'parser': subparsers.add_parser(name='import-json', help='Import data from JSON file (requires admin privileges).'),
        },
        # 'user': {
        #     'route': user_cli_router,
        #     'parser': subparsers.add_parser(name='user', help='Add/edit/remove users (requires admin privileges).'),
        # },
    }

    cli_switch_router(argparser, switcher, router_args)
