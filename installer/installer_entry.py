import os
import sys


def get_user():
    try:
        import pwd
    except ImportError:
        import getpass
        pwd = None

    if pwd:
        return pwd.getpwuid(os.geteuid()).pw_name
    else:
        return getpass.getuser()


active_user = get_user()
if active_user == 'root':
    os.chdir(sys._MEIPASS)
    os.system(f"./binaries/watchbot_installer {active_user}")
else:
    os.chdir(sys._MEIPASS)
    print('This installation requires sudo permissions.')
    os.system(f"sudo ./binaries/watchbot_installer {active_user}")
