abstract_meta_schema = {
    'id': {
        'type': 'text',
        'null': False,
        'default': None,
        'unique': False,
        'primary_key': True
    },
    'timestamp': {
        'type': 'text',
        'null': False,
        'default': None,
        'unique': False,
    }
}

watchbot_schema = {
    'watch': {
        **abstract_meta_schema,
        'path': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'type': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'ext': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        },
        'recursive': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        },
        'user': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        },
        'sudo': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        }
    },
    'trigger': {
        **abstract_meta_schema,
        'watch_id': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'event': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        },
        'work_dir': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'command': {
            # use variable names to pass in values from trigger (like path/filename/trigger type)
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'user': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        },
        'sudo': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        }
    },
    'log': {
        **abstract_meta_schema,
        'trigger_id': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'stdout': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'stderr': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'exit_code': {
            'type': 'text',
            'null': False,
            'default': None,
            'unique': False,
        },
        'user': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        },
        'sudo': {
            'type': 'text',
            'null': True,
            'default': None,
            'unique': False,
        }
    }
}

field_defaults = {
    'null': True,
    'default': None,
    'unique': False,
    'primary_key': False
}

sqlite_generated_tables = [
    'sqlite_sequence'
]